
var my_news = [
  {
    author: 'Sasha',
    text: 'Hello My name Sasha!...',
    bigText: 'Hello My name Sasha! Mmmmm'
  },
  {
    author: "Pasha",
    text: "I'm learning how backend...",
    bigText: "I'm learning how backend should work with That piece of shit - REACT!"
  },
  {
    author: "Masha",
    text: "I'm making design for...",
    bigText: "I'm making design for our future App and I want start writing programs on JavaScript"
  },
  {
    author: 'Tanya',
    text: "I'm learning how react...",
    bigText: "I'm learning how react-app is working and I'm trying understand it"
  },
];

var Article = React.createClass({

  propTypes: {

    data: React.PropTypes.shape({

      author: React.PropTypes.string.isRequired,
      text: React.PropTypes.string.isRequired,
      bigText:  React.PropTypes.string.isRequired,

    })

  },
  getInitialState: function() {
    return {
      visible: false,
      rating: 0,
      eshe_odno_svoistvo: 'qweqwe',
    }
  },
  showMoreText: function(e) {
    e.preventDefault();
    // var visibleState = (this.props.visible ? false : true);
    this.setState({
      visible: true,
      rating: 555,
      eshe_odno_svoistvo: 'hello'
    }, function() {
      alert("rating has been changed!");
    })
  },
  render: function() {

    var articleItem = this.props.data;
    var dynamicItem = this.state;

    var author = articleItem.author;
    var text = articleItem.text;
    var bigText = articleItem.bigText;

    var visible = dynamicItem.visible;

    console.log('render', this);

    return (
      <div className="article">
        <p className="article__author">{author}:</p>

        <a href="#"
          onClick={this.showMoreText}

          className={'article__show-more-text ' +
            (visible ? 'none': '')
          }>
          Tap On Me! </a>
        <p className={
            "article__text " + (visible ? 'none': '')
          }>{text}</p>

        <p className={
            "article__big-text " + (visible ? '': 'none')
          }>{bigText}</p>
      </div>
    );

  }

});

var News = React.createClass({
  propTypes: {
    news_data: React.PropTypes.array.isRequired,
  },
  getInitialState: function() {
    return {
      click: 0,
    }
  },
  render: function() {

    var news = this.props.news_data;
    var newsCounter = news.length;

    var allNews;

    if (newsCounter > 0) {
      allNews = news.map(function(item, index) {
        return (
          <div key={index}>
            <Article data={item} />
          </div>
        )

      });
    } else {
      allNews = <p>Новостей нет! Удачного дня!</p>
    }

    // console.log(allNews);

    var newsCounterClass = newsCounter > 0 ? '': 'none';

    return (
      <div className="news">
          {allNews}
          <strong className = {
              "news__counter " + newsCounterClass
            }> All News = {newsCounter} </strong>
      </div>
    );
  }
});

var TestInput = React.createClass({

  componentDidMount: function() { // фокус в инпут
    ReactDOM.findDOMNode(this.refs.myTestInput).focus();
  },
  componentWillReceiveProps: function(nextProp) {
    this.setState({
      likesIncreasing: nextProps.likeCount > this.props.likeCount
    });
  },
  inputChanged: function(e){
    e.preventDefault();

    console.log("inputChanged", this);

    this.setState ({
      inputValue: e.target.value
    })
  },
  getInitialState: function() {
    return {
      inputValue: "test..."
    }
  },
  showInputInAlert: function() {
    console.log(this.refs);
    alert(ReactDOM.findDOMNode(
      this.refs.myTestInput).value);
  },
  render: function() {

    var inputValue = this.state.inputValue;

    return (
      <div className='input'>
        <input
          className='input-window'
          defaulValue=''
          placeholder='type something ...'
          ref='myTestInput'/>
        <button onClick={this.showInputInAlert}
          className='input-btn'>
          Tap
        </button>
      </div>
    );
  }
});

var Add = React.createClass({

  getInitialState: function() { // устанавливаем начальное значение state
    return {
      validateBtn: false,
      validateAuthor: false,
      validateText: false
    };
  },

  componentDidMount: function() {
    ReactDOM.findDOMNode(this.refs.author).focus();
  },

  onCheckValidateBtn: function(e) {
    this.setState({validateBtn: !this.state.validateBtn}); // toggle btn
  },

  onChangeValidateAuthor: function(e) {
    e.preventDefault();

    var author = ReactDOM.findDOMNode(this.refs.author).value.trim();
    if (author.length > 0) {
      console.log("text Length", author.length);
      this.setState({validateAuthor: true});
    } else {
      this.setState({validateAuthor: false});
    }
  },

  onChangeValidateText: function(e) {
    e.preventDefault();

    var text = ReactDOM.findDOMNode(this.refs.text).value.trim();
    if (text.length > 0) {
      console.log("text Length", text.length);
      this.setState({validateText: true});
    } else {
      this.setState({validateText: false});
    }
  },

  onBtnClickHandler: function(e) {
    e.preventDefault();

    var validateBtn = this.state.validateBtn;
    var validateText = this.state.validateText;
    var validateAuthor = this.state.validateAuthor;

    if ((validateText == true) &&
      (validateAuthor == true) &&
      (validateBtn == true)) {

      var author = ReactDOM.findDOMNode(this.refs.author).value;
      var text = ReactDOM.findDOMNode(this.refs.text).value;
      alert(author + "\n" + text);

    } else {

      alert("Enter Data, please)");

    }


  },

  render: function() {


    return (
      <form className="add cf">
        <input
          type='text'
          className="add__author"
          defaulValue=''
          placeholder='Your name'
          ref='author'
          onChange={this.onChangeValidateAuthor}
        />
        <textarea
          className='add__text'
          defaulValue=''
          placeholder='news text'
          ref='text'
          onChange={this.onChangeValidateText}
          ></textarea>
        <label className='add__checkrule'>
          <input
            type='checkbox'
            defaulChecked={false}
            ref='checkrule'
            onChange={this.onCheckValidateBtn}
            /> I agree with all rulses
        </label>

        {/*берем значение disabled атрибута из state*/}
        <button
          className="add__btn"
          onClick={this.onBtnClickHandler}
          ref='alert_button'
          disabled={!this.state.validateBtn}>
          Show Alert!
        </button>
      </form>
    );
  }

});

var App = React.createClass({
  render: function() {
    return (
      <div className="app">
        <h3>News</h3>
        {/*<TestInput />*/}
        <Add />
        <News news_data={my_news} />
      </div>
    );
  }
});

ReactDOM.render(
    <App />,
    document.getElementById('root')
);
